#include <iostream>
#include "string"

int main()
{
	std::string color = "Black";
	std::cout << color << std::endl;
	std::cout << color.length() << std::endl;
	std::cout << color[0] << std::endl;
	std::cout << color[4] << std::endl;

	return 0;

}